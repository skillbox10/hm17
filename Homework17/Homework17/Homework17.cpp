﻿#include <iostream>
#include <cmath>

class Vector {
    private:
        double x;
        double y;
        double z;
    
    public:
        Vector(): x(0), y(0), z(0)
        {}

        Vector(double _x, double _y, double _z) {
            x = _x;
            y = _y;
            z = _z;
        }

        void getX() {
            std::cout << x;

        }
        void getY() {
            std::cout << x;

        }
        void getZ() {
            std::cout << x;

        }
        void setX(double _x) {
            x = _x;

        }
        void setY(double _y) {
            y = _y;

        }
        void setZ(double _z) {
            z = _z;

        }

        double getVectorLength() {
            double sum = x * x + y * y + z * z;
            return std::sqrt(sum);
        }

};

int main()
{
    Vector vect(10, 10, 10);
    double answ = vect.getVectorLength();
    std::cout << answ;
}
